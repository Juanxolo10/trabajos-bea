#!/bin/bash

read -p "Escribe un número: " num

for (( i=0; i<=$num; i++ ))
do
	let par=i%2
	
	if [[ par -eq 0 ]]
	then
		echo "$i es número par"
	else
		echo "$i es número impar"
	fi
done
