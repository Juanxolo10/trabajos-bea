#!/bin/bash

read -p "Escribe un nombre para un directorio: " dir

if [[ -d $dir ]];
then
	echo "$dir existe"
else
	mkdir $dir
	echo "Hemos creado $dir"
fi

read -p "Escribe un fichero: " fichero1
read -p "Escribe un fichero: " fichero2

if [[ -f $fichero1 && -f $fichero2 ]];
then
	echo "Los 2 ficheros existen"
else
	cd $dir
	touch $fichero1 $fichero2
	echo "Hemos creado $fichero1 y $fichero2"
fi
